<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
</head>
<body>
    <h1>Đăng nhập</h1>
    <form>
        <label for="username">Tên đăng nhập:</label>
        <input type="text" id="username" name="username"><br><br>
        
        <label for="password">Mật khẩu:</label>
        <input type="password" id="password" name="password"><br><br>
        
        <input type="submit" value="Đăng nhập">
    </form>
    
    <?php
    // Hiển thị ngày/giờ hiện tại theo timezone Việt Nam
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    echo '<p>Ngày/giờ hiện tại: ' . date('d/m/Y H:i:s') . '</p>';
    ?>
</body>
</html>
