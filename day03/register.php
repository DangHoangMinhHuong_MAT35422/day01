<!DOCTYPE html>
<html>
<head>
    <style>
        .outer-box {
            border: 1px solid #ccc;
            padding: 20px;
            width: 400px;
            margin: 0 auto;
        }

        .form-group {
            display: flex;
            align-items: center;
            justify-content: space-between; 
            margin-bottom: 20px;
            
        }

        .label {
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
            flex-basis: 30%;
        }

        .text {
            flex-basis: 40%;
        }

        select {
            flex-basis: 30%;
            padding: 14px;
            border: 2px solid #648bae;
            box-sizing: border-box;
            border-radius: 1px;
        }

        input[type="text"] {
            width: 100%;
            padding: 14px;
            border: 2px solid #648bae;
            box-sizing: border-box;
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 25%;
            padding: 15px;
            border: 2px solid #648bae;
            background-color: #4CAF50;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
    </style>
</head>
<body>
    <div class="outer-box">
        <form action="process_registration.php" method="POST">
            <div class="form-group">
                <div class="label">Họ và tên:</div>
                <div class="text">
                    <input type="text" id="fullname" name="fullname" required>
                </div>
            </div>

            <div class="form-group">
                <div class="label">Giới tính:</div>
                <div class="text">
                    <input type="radio" id="gender_male" name="gender" value="Nam" required>
                    <label for="gender_male">Nam</label>
                    <input type="radio" id="gender_female" name="gender" value="Nữ" required>
                    <label for="gender_female">Nữ</label>
                </div>
            </div>

            <div class="form-group">
                <div class="label">Phân khoa:</div>
                <select id="nganh" name="nganh" required>
                    <option value="">--Chọn phân khoa--</option>
                    <option value="Khoa học Máy tính">Khoa học Máy tính</option>
                    <option value="Khoa học Vật liệu">Khoa học Vật liệu</option>
                </select>
            </div>

            <br><br>
            <button>Đăng ký</button>
        </form>
    </div>
</body>
</html>