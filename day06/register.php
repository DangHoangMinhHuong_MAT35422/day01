<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký</title>
    <style>
        .error {
            color: red;
        }

        .outer-box {
            background-color: black;
            padding: 2px;
            width: max-content;
            margin: 0 auto;
        }

        .form-box {
            background-color: white;
            padding: 20px;
            width: 400px;
            margin: 0 auto;
        }

        .form-field {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
        }

        .form-label {
            width: 40%;
            font-size: 18px;
            color: white;
            background-color: green;
            padding: 5px;
            margin-right: 10px;
            text-align: center;
        }

        .form-field input,
        .form-field select {
            width: 70%;
        }

        .button-box {
            text-align: center;
            margin-top: 10px;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#registerForm').submit(function(event){
                event.preventDefault(); // Prevent form submission

                // Reset error messages
                $('.error').text('');

                // Perform form validation
                var name = $('#name').val();
                var department = $('#department').val();
                var birthday = $('#birthday').val();
                var address = $('#address').val();
                var image = $('#image').prop('files')[0];

                var valid = true;
                var errorMessage = '';

                if (name === '') {
                    errorMessage += 'Hãy nhập tên.<br>';
                    valid = false;
                    $('#nameError').text('.');
                }

                if (department === '') {
                    errorMessage += 'Hãy chọn phân khoa.<br>';
                    valid = false;
                    $('#departmentError').text('.');
                }

                if (birthday === '') {
                    errorMessage += 'Hãy nhập ngày sinh.<br>';
                    valid = false;
                    $('#birthdayError').text('.');
                } else {
                    var pattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                    if (!pattern.test(birthday)) {
                        errorMessage += 'Hãy nhập ngày sinh đúng định dạng.<br>';
                        valid = false;
                        $('#birthdayError').text('.');
                    }
                }

                if (address === '') {
                    errorMessage += 'Hãy nhập địa chỉ.<br>';
                    valid = false;
                    $('#addressError').text('.');
                }

                if (!image) {
                    errorMessage += 'Hãy chọn hình ảnh.<br>';
                    valid = false;
                    $('#imageError').text('.');
                }

                // Display error messages
                $('#errorMessage').html(errorMessage);

                // Submit the form if valid
                if (valid) {
                    var formData = new FormData(this);
                    formData.append('image', image);
                    $.ajax({
                        url: 'confirm.php',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            // Hiển thị thông báo thành công
                            alert(response);

                            // Chuyển hướng đến trang xác nhận
                            window.location.href = "confirm.php";
                        }
                    });
                }
            });
        });
    </script>
</head>
<body>
    <div class="outer-box">
        <div class="form-box">
            <form id="registerForm" method="POST" action="confirm.php" enctype="multipart/form-data">
                <div id="errorMessage" class="error"></div>
                <div class="form-field">
                    <label class="form-label" for="name">Họ và tên:</label>
                    <input type="text" id="name" name="name">
                    <span class="error" id="nameError"></span>
                </div>
                <div class="form-field">
                    <label class="form-label">Giới tính:</label>
                    <input type="radio" id="genderMale" name="gender" value="Nam">
                    <label for="genderMale">Nam</label>
                    <input type="radio" id="genderFemale" name="gender" value="Nữ">
                    <label for="genderFemale">Nữ</label>
                </div>
                <div class="form-field">
                    <label class="form-label" for="department">Phân khoa:</label>
                    <select id="department" name="department">
                        <option value="">--Chọn phân khoa--</option>
                        <option value="Khoa A">Khoa A</option>
                        <option value="Khoa B">Khoa B</option>
                        <option value="Khoa C">Khoa C</option>
                    </select>
                    <span class="error" id="departmentError"></span>
                </div>
                <div class="form-field">
                    <label class="form-label" for="birthday">Ngày sinh:</label>
                    <input type="text" id="birthday" name="birthday" placeholder="dd/mm/yyyy">
                    <span class="error" id="birthdayError"></span>
                </div>
                <div class="form-field">
                    <label class="form-label" for="address">Địa chỉ:</label>
                    <input type="text" id="address" name="address">
                    <span class="error" id="addressError"></span>
                </div>
                <div class="form-field">
                    <label class="form-label" for="image">Hình ảnh:</label>
                    <input type="file" id="image" name="image">
                    <span class="error" id="imageError"></span>
                </div>
                <div class="button-box">
                    <button type="submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>