<!-- search.php -->
<!DOCTYPE html>
<html>
<head>
    <title>Tìm kiếm sinh viên</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            margin-bottom: 20px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
        }

        th {
            background-color: #f2f2f2;
        }

        .form-field {
            margin-bottom: 10px;
        }

        .form-label {
            display: inline-block;
            width: 100px;
        }

        .button-box {
            margin-top: 10px;
        }

        .count-box {
            margin-top: 10px;
            font-weight: bold;
        }

        .edit-button, .delete-button {
            background-color: #008CBA;
            color: white;
            border: none;
            padding: 5px 10px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            cursor: pointer;
            margin-right: 5px;
        }
    </style>
</head>
<body>
    <?php
    // Mock data for demonstration
    $students = [
        [
            'name' => 'Nguyen Van A',
            'birthday' => '1990-01-01',
            'department' => 'Khoa A',
            'address' => 'Hanoi'
        ],
        [
            'name' => 'Tran Thi B',
            'birthday' => '1992-05-10',
            'department' => 'Khoa B',
            'address' => 'Ho Chi Minh City'
        ],
        [
            'name' => 'Pham Van C',
            'birthday' => '1995-08-22',
            'department' => 'Khoa C',
            'address' => 'Da Nang'
        ]
    ];

    if (isset($_GET['khoa']) && isset($_GET['tuKhoa'])) {
        $khoa = $_GET['khoa'];
        $tuKhoa = $_GET['tuKhoa'];

        // Filter students based on search criteria
        $filteredStudents = array_filter($students, function($student) use ($khoa, $tuKhoa) {
            return (strpos($student['department'], $khoa) !== false) &&
                   (strpos($student['name'], $tuKhoa) !== false);
        });

        $students = $filteredStudents;
    }
    ?>

    <h1>Tìm kiếm sinh viên</h1>
    <form id="searchForm">
        <div class="form-field">
            <label class="form-label" for="khoa">Khoa:</label>
            <input type="text" id="khoa" name="khoa">
        </div>
        <div class="form-field">
            <label class="form-label" for="tuKhoa">Từ khóa:</label>
            <input type="text" id="tuKhoa" name="tuKhoa">
        </div>
        <div class="button-box">
            <button type="reset">Reset</button>
        </div>
    </form>

    <h2>Số sinh viên tìm thấy: <span id="resultCount"><?php echo count($students); ?></span></h2>
    <table id="resultTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="resultBody">
            <?php foreach ($students as $student): ?>
                <tr>
                    <td><?php echo $student['name']; ?></td>
                    <td><?php echo $student['birthday']; ?></td>
                    <td><?php echo $student['department']; ?></td>
                    <td><?php echo $student['address']; ?></td>
                    <td>
                        <button class="edit-button">Sửa</button>
                        <button class="delete-button">Xóa</button>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#searchForm').submit(function(event) {
                event.preventDefault(); // Prevent form submission

                // Get search criteria from form
                var khoa = $('#khoa').val();
                var tuKhoa = $('#tuKhoa').val();

                // Send AJAX request to search.php with search criteria
                $.ajax({
                    url: 'search.php',
                    type: 'GET',
                    data: {khoa: khoa, tuKhoa: tuKhoa},
                    success: function(response) {
                        // Update search results and count
                        $('#resultBody').html(response);
                        $('#resultCount').text($('#resultTable tbody tr').length);
                    },
                    error: function(xhr, status, error) {
                        console.log(error);
                    }
                });
            });

            $('.edit-button').click(function() {
                // Handle edit button click
                var studentName = $(this).closest('tr').find('td:nth-child(1)').text();
                // Perform edit operation for the student
                console.log('Edit: ' + studentName);
            });

            $('.delete-button').click(function() {
                // Handle delete button click
                var studentName = $(this).closest('tr').find('td:nth-child(1)').text();
                // Perform delete operation for the student
                console.log('Delete: ' + studentName);
            });
        });
    </script>
</body>
</html>