<?php
// Thông tin kết nối đến cơ sở dữ liệu
$servername = "localhost";
$username = "your_username";
$password = "your_password";
$dbname = "ltweb";

// Tạo kết nối đến cơ sở dữ liệu
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}

// Lấy dữ liệu từ biến POST
$name = $_POST['name'];
$gender = $_POST['gender'];
$department = $_POST['department'];
$birthday = $_POST['birthday'];
$address = $_POST['address'];
$image = $_FILES['image']['name'];

// Chuẩn bị câu truy vấn INSERT
$sql = "INSERT INTO students (name, gender, department, birthday, address, image) 
        VALUES ('$name', '$gender', '$department', '$birthday', '$address', '$image')";

// Thực thi câu truy vấn
if ($conn->query($sql) === TRUE) {
    echo "Đăng ký thành công!";
} else {
    echo "Lỗi: " . $conn->error;
}

// Đóng kết nối đến cơ sở dữ liệu
$conn->close();
?>