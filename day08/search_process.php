<?php
// Tạo kết nối đến cơ sở dữ liệu
$servername = "localhost";
$username = "your_username";
$password = "your_password";
$dbname = "ltweb";
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Kết nối thất bại: " . $conn->connect_error);
}

// Lấy dữ liệu từ biến POST
$khoa = $_POST['khoa'];
$tuKhoa = $_POST['tuKhoa'];

// Chuẩn bị câu truy vấn SELECT
$sql = "SELECT * FROM students WHERE khoa LIKE '%$khoa%' AND (name LIKE '%$tuKhoa%' OR department LIKE '%$tuKhoa%')";

// Thực thi câu truy vấn
$result = $conn->query($sql);

// Xử lý kết quả trả về
if ($result->num_rows > 0) {
    $output = "";
    $count = 1;
    while ($row = $result->fetch_assoc()) {
        $output .= "<tr>";
        $output .= "<td>" . $count . "</td>";
        $output .= "<td>" . $row['name'] . "</td>";
        $output .= "<td>" . $row['khoa'] . "</td>";
        $output .= "<td>";
        $output .= "<button onclick='deleteStudent(" . $row['id'] . ")'>Xóa</button>";
        $output .= "<button onclick='editStudent(" . $row['id'] . ")'>Sửa</button>";
        $output .= "</td>";
        $output .= "</tr>";
        $count++;
    }
    echo $output;
} else {