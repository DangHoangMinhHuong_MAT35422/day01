<!DOCTYPE html>
<html>
<head>
    <title>Form đăng ký sinh viên</title>
    <style>
        .form-container {
            width: 400px;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin: 0 auto;
            background-color: #f0f8ff;
        }
        
        .form-container h1 {
            text-align: center;
        }
        
        .form-container .form-box {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            margin-bottom: 10px;
            padding: 10px;
            background-color: #e0f2f1;
        }
        
        .form-container .form-box label {
            font-weight: bold;
            display: inline-block;
            vertical-align: middle;
        }
        
        .form-container .form-box input[type="text"],
        .form-container .form-box select {
            width: 100%;
            padding: 5px;
            border-radius: 3px;
        }
        
        .form-container .form-box select {
            background-image: url("https://cdn.jsdelivr.net/npm/heroicons@1.0.0/icons/solid/chevron-down.svg");
            background-repeat: no-repeat;
            background-position: right 5px center;
            padding-right: 20px;
        }
        
        .form-container textarea {
            width: 100%;
            padding: 5px;
            border-radius: 3px;
        }
        
        .form-container input[type="submit"] {
            background-color: #4caf50;
            color: white;
            padding: 8px 16px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        
        .form-container input[type="submit"]:hover {
            background-color: #45a049;
        }
        
        .form-container .error-message {
            color: red;
            margin-top: 5px;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h1>Form đăng ký sinh viên</h1>
        <form action="regist_student.php" method="POST">
            <div class="form-box">
                <label for="name">Họ và tên:</label>
                <input type="text" id="name" name="name" value="">
            </div>
            
            <div class="form-box">
                <label for="gender">Giới tính:</label>
                <input type="radio" id="gender1" name="gender" value="1">
                <label for="gender1">Nam</label>
                <input type="radio" id="gender2" name="gender" value="2">
                <label for="gender2">Nữ</label>
            </div>
            
            <div class="form-box">
                <label for="dob">Ngày sinh:</label>
                <select id="dob" name="dob">
                    <?php
                        $currentYear = date("Y");
                        $startYear = $currentYear - 40;
                        $endYear = $currentYear - 15;
                        
                        for ($year = $startYear; $year <= $endYear; $year++) {
                            for ($month = 1; $month <= 12; $month++) {
                                for ($day = 1; $day <= 31; $day++) {
                                    $date = sprintf("%02d-%02d-%04d", $day, $month, $year);
                                    echo "<option value=\"$date\">$date</option>";
                                }
                            }
                        }
                    ?>
                </select>
            </div>
            
            <div class="form-box">
                <label for="city">Địa chỉ:</label>
                <label for="city">Thành phố:</label>
                <select id="city" name="city" onchange="loadDistricts()">
                    <option value="">-- Chọn thành phố --</option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="Tp Hồ Chí Minh">Tp.Hồ Chí Minh</option>
                </select>
                
                <label for="district">Quận:</label>
                <select id="district" name="district"></select>
            </div>
            
            <div class="form-box">
                <textarea id="other" name="other"></textarea>
            </div>
            
            <div class="form-box">
                <input type="submit" value="Đăng ký">
            </div>
            
            <div class="form-box">
                <p class="error-message" id="error-message"></p>
            </div>
        </form>
    </div>
    
    <script>
        function loadDistricts() {
            var citySelect = document.getElementById("city");
            var districtSelect = document.getElementById("district");
            var selectedCity = citySelect.options[citySelect.selectedIndex].value;

            districtSelect.innerHTML = "";

            if (selectedCity === "Hà Nội") {
                var districts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
            } else if (selectedCity === "Tp Hồ Chí Minh") {
                var districts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
            }

            for (var i = 0; i < districts.length; i++) {
                var option = document.createElement("option");
                option.text = districts[i];
                option.value = districts[i];
                districtSelect.add(option);
            }
        }

        // Kiểm tra các trường bắt buộc trước khi submit form
        document.querySelector("form").addEventListener("submit", function(event) {
            var nameInput = document.getElementById("name");
            var genderInputs = document.querySelectorAll("input[name='gender']");
            var dobSelect = document.getElementById("dob");
            var citySelect = document.getElementById("city");
            var districtSelect = document.getElementById("district");

            var errorMessage = "";
            
            if (nameInput.value === "") {
                errorMessage += "Vui lòng nhập họ và tên.\n";
            }
            
            var isGenderSelected = false;
            for (var i = 0; i < genderInputs.length; i++) {
                if (genderInputs[i].checked) {
                    isGenderSelected = true;
                    break;
                }
            }
            if (!isGenderSelected) {
                errorMessage += "Vui lòng chọn giới tính.\n";
            }
            
            if (dobSelect.value === "") {
                errorMessage += "Vui lòng chọn ngày sinh.\n";
            }
            
            if (citySelect.value === "") {
                errorMessage += "Vui lòng chọn thành phố.\n";
            }
            
            if (districtSelect.value === "") {
                errorMessage += "Vui lòng chọn quận.\n";
            }

            if (errorMessage !== "") {
                document.getElementById("error-message").textContent = errorMessage;
                event.preventDefault(); // Ngăn không cho form submit
            }
        });
    </script>
</body>
</html>