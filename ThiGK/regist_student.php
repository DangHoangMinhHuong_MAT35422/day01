<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST["name"];
    $gender = $_POST["gender"];
    $dob = $_POST["dob"];
    $city = $_POST["city"];
    $district = $_POST["district"];
    $other = $_POST["other"];
    
    echo "<style>";
    echo ".info-container {";
    echo "    width: 400px;";
    echo "    padding: 20px;";
    echo "    border: 1px solid #ccc;";
    echo "    border-radius: 5px;";
    echo "    margin: 0 auto;";
    echo "    background-color: #f0f8ff;";
    echo "}";

    echo ".info-container h1 {";
    echo "    text-align: center;";
    echo "}";

    echo ".info-box {";
    echo "    margin-bottom: 10px;";
    echo "    padding: 10px;";
    echo "    background-color: #e0f2f1;";
    echo "}";

    echo ".info-box label {";
    echo "    font-weight: bold;";
    echo "}";

    echo ".info-value {";
    echo "    margin-top: 5px;";
    echo "}";

    echo "</style>";

    echo "<div class='info-container'>";
    echo "<h1>Thông tin đăng ký</h1>";

    echo "<div class='info-box'>";
    echo "<label for='name'>Họ và tên:</label>";
    echo "<div class='info-value'>$name</div>";
    echo "</div>";

    echo "<div class='info-box'>";
    echo "<label for='gender'>Giới tính:</label>";
    echo "<div class='info-value'>";
    echo ($gender == 1) ? "Nam" : "Nữ";
    echo "</div>";
    echo "</div>";

    echo "<div class='info-box'>";
    echo "<label for='dob'>Ngày sinh:</label>";
    echo "<div class='info-value'>$dob</div>";
    echo "</div>";

    echo "<div class='info-box'>";
    echo "<label for='city'>Địa chỉ:</label>";
    echo "<div class='info-value'>$district, $city</div>";
    echo "</div>";

    echo "<div class='info-box'>";
    echo "<label for='other'>Thông tin khác:</label>";
    echo "<div class='info-value'>$other</div>";
    echo "</div>";

    echo "</div>"; // Kết thúc info-container
}
?>