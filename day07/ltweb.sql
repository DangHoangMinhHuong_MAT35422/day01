CREATE DATABASE ltweb;

USE ltweb;

CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    gender VARCHAR(10),
    department VARCHAR(50),
    birthday DATE,
    address VARCHAR(255),
    image VARCHAR(255)
);