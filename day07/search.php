<?php
// Kết nối tới cơ sở dữ liệu
// Thay đổi thông tin kết nối tương ứng với cấu hình của bạn
$servername = "localhost";
$dbname = "database";

$conn = new mysqli($servername, $username, $password, $dbname);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối tới cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Lấy dữ liệu từ request
$khoa = $_GET['khoa'];
$tuKhoa = $_GET['tuKhoa'];

// Xử lý truy vấn
$sql = "SELECT * FROM students WHERE department = '$khoa' AND (name LIKE '%$tuKhoa%' OR address LIKE '%$tuKhoa%')";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $students = array();
    while ($row = $result->fetch_assoc()) {
        $students[] = $row;
    }
    echo json_encode($students);
} else {
    echo "Không tìmthấy kết quả phù hợp.";
}
$conn->close();
?>