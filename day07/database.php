<?php
$servername = "localhost";
$username = "";
$password = "";
$dbname = "ltweb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Chuẩn bị câu lệnh INSERT
$sql = "INSERT INTO students (name, birthday, department, address)
        VALUES ('John Doe', '2000-01-01', 'Computer Science', '123 Street, City')";

// Thực thi câu lệnh INSERT
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

// Đóng kết nối
$conn->close();
?>