<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Lấy dữ liệu từ biểu mẫu đăng ký
    $name = $_POST['name'];
    $gender = $_POST['gender'];
    $department = $_POST['department'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    // Kiểm tra xem đã tải lên hình ảnh chưa
    if (isset($_FILES['image']) && $_FILES['image']['error'] === UPLOAD_ERR_OK) {
        $image = $_FILES['image']['name'];
        $targetDirectory = "upload";
        $targetFilePath = $targetDirectory . $image;

        // Di chuyển tệp tải lên vào thư mục đích
        if (move_uploaded_file($_FILES['image']['tmp_name'], $targetFilePath)) {
            // Đăng ký thành công
            $successMessage = "Đăng ký thành công!";
            echo $successMessage;
        } else {
            $errorMessage = "Đã xảy ra lỗi khi di chuyển hình ảnh tải lên.";
            echo $errorMessage;
        }
    } else {
        $errorMessage = "Đã xảy ra lỗi khi tải lên hình ảnh.";
        echo $errorMessage;
    }
} else {
    $errorMessage = "Phương thức không hợp lệ.";
    echo $errorMessage;
}