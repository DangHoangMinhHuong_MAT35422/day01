const fileInput = document.getElementById("image");
const selectedImage = document.getElementById("selected-image");
const customFileUpload = document.querySelector(".custom-file-upload");
const fileNameSpan = document.getElementById("file-name");

fileInput.addEventListener("change", function () {
    if (fileInput.files.length > 0) {
        const reader = new FileReader();
        reader.onload = function (e) {
            selectedImage.src = e.target.result;
            selectedImage.style.display = "block";  
            customFileUpload.style.display = "none";
            fileNameSpan.style.display = "none";
            const label = fileInput.nextElementSibling; 
            label.querySelector("span").textContent = fileInput.files[0].name; 
        };
        reader.readAsDataURL(fileInput.files[0]);
    } else {
        selectedImage.src = "";
        selectedImage.style.display = "none";
        const label = fileInput.nextElementSibling;  
        label.querySelector("span").textContent = " Chưa chọn ảnh"; 
    }
});

$(document).ready(function() {
    $("#submit-button").click(function() {
        var fullName = $("#full-name-input").val();
        var genderSelected = $("input[name='gender']:checked").length > 0;
        var department = $("#department").val();
        var dateOfBirth = $("#date-of-birth-input").val();
        var errorMessage = $("#error-message");

        errorMessage.html(""); 

        var errors = [];

        if (fullName.trim() === "") {
            errors.push("Hãy nhập tên.");
        }

        if (!genderSelected) {
            errors.push("Hãy chọn giới tính.");
        }

        if (department === "") {
            errors.push("Hãy chọn phân khoa.");
        }

        if (dateOfBirth.trim() === "") {
            errors.push("Hãy nhập ngày sinh.");
        } else {        
            var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
            if (!dateRegex.test(dateOfBirth)) {
                errors.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
            }
        }

        if (errors.length === 0) {
            $("#hidden-full-name").val(fullName);
            $("#hidden-gender").val($("input[name='gender']:checked").val());
            $("#hidden-department").val(department);
            $("#hidden-date-of-birth").val(dateOfBirth);
            $("#hidden-address").val($("#address").val());

            const fileInput = document.getElementById("image");
            if (fileInput.files.length > 0) {
                const file = fileInput.files[0];
                const reader = new FileReader();
                reader.onload = function (e) {
                    $("#hidden-image").val(e.target.result);
                    $("#registration-form").submit();
                };
                reader.readAsDataURL(file);
            } else {
                $("#registration-form").submit();
            }
        } else {
            errorMessage.html(errors.join("<br>")); 
        }
    });
});
       
