$(document).ready(function () {
    $("form").submit(function (e) {
        e.preventDefault();

        var department = $("#getDept").val();
        var keyword = $("#getName").val();

        $.ajax({
            type: "POST",
            url: "search.php", // Đổi đường dẫn tới file xử lý tìm kiếm
            data: {
                depts: department,
                search: keyword
            },
            success: function (response) {
                var resultCount = (response.match(/<tr>/g) || []).length - 1;
                $("#resultCount").text("Số sinh viên được tìm thấy: " + resultCount);
                $("#no").html(response);
            }
        });
    });
});
