CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(255) NOT NULL,
    gender VARCHAR(10) NOT NULL,
    department VARCHAR(10) NOT NULL,
    date_of_birth DATE NOT NULL,
    address VARCHAR(255),
    image VARCHAR(255)
);