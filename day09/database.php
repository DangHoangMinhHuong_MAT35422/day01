<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $department = $_POST['depts'] ?? '';
    $searchKeyword = $_POST['search'] ?? '';

    $sql = "SELECT * FROM students WHERE department = :department AND full_name LIKE :searchKeyword";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':department', $department, PDO::PARAM_STR);
    $stmt->bindValue(':searchKeyword', "%$searchKeyword%", PDO::PARAM_STR);
    $stmt->execute();

    if ($stmt->rowCount() > 0) {
        echo "<table>";
        echo "<tr><th>ID</th><th>Tên sinh viên</th><th>Khoa</th><th>Action</th></tr>";

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo "<tr>";
            echo "<td>{$row['id']}</td>";
            echo "<td>{$row['full_name']}</td>";
            echo "<td>{$row['department']}</td>";
            echo "<td class='action'><a href='update_students.php?id={$row['id']}'>Sửa</a> | <a href='delete.php?id={$row['id']}'>Xóa</a></td>";
            echo "</tr>";
        }          

        echo "</table>";
    } else {
        echo "Không có sinh viên nào được tìm thấy.";
    }
} catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>
