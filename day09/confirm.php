<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirmation Page</title>
    <link rel="stylesheet" href="./styles/styles_confirm.css">
</head>
<body>
    <div class="form-container">
        <?php
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $fullName = $_POST["full-name"];
            $gender = $_POST["gender"];
            $department = $_POST["department"];
            $dateOfBirth = $_POST["date-of-birth"];
            $address = $_POST["address"];
            $image = $_POST["image"];

            $departmentMap = array(
                "MAT" => 'Khoa học máy tính',
                "KDL" => 'Khoa học vật liệu'
            );

            displayRegistrationInfo("Họ và tên:", $fullName);
            displayRegistrationInfo("Giới tính:", $gender);
            displayRegistrationInfo("Phân khoa:", $departmentMap[$department]);
            displayRegistrationInfo("Ngày sinh:", $dateOfBirth);
            displayRegistrationInfo("Địa chỉ:", $address);

            if (!empty($image)) {
                displayImage("Hình ảnh:", $image);
            }
        } else {
            echo "<p>Không có thông tin đăng ký để hiển thị.</p>";
        }
        ?>
        <button type="button" id="submit-button">Xác nhận</button>
        <?php
            saveRegistrationToDatabase();
        ?>
    </div>
</body>
</html>


<?php
function displayRegistrationInfo($label, $value) {
    echo "<div class='register-form'>";
    echo "<label class='form-label'>$label</label>";
    echo "<input type='text' value='$value' readonly>";
    echo "</div>";
}

function displayImage($label, $imageUrl) {
    echo "<div class='register-form'>";
    echo "<label for='image' class='form-label'>$label</label>";
    echo "<img src='$imageUrl' alt='Hình ảnh đăng ký' style='max-width: 150px;'>";
    echo "</div>";
}

function saveRegistrationToDatabase() {
    require_once "database.php";

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $fullName = $_POST["full-name"] ?? '';
        $gender = $_POST["gender"] ?? '';
        $department = $_POST["department"] ?? '';
        $dateOfBirth = $_POST["date-of-birth"] ?? '';
        $address = $_POST["address"] ?? '';
        $image = $_POST["image"] ?? '';

        // Check if all required fields are set before saving to the database
        if ($fullName && $gender && $department && $dateOfBirth && $address) {
            $sql = "INSERT INTO students (full_name, gender, department, date_of_birth, address, image) 
                    VALUES ('$fullName', '$gender', '$department', '$dateOfBirth', '$address', '$image')";

            try {
                $conn->exec($sql);
                echo "<p>Đã lưu thông tin đăng ký vào database.</p>";
            } catch (PDOException $e) {
                echo "<p>Lỗi: " . $e->getMessage() . "</p>";
            }
        } else {
            echo "<p>Không đủ thông tin để lưu vào database.</p>";
        }
    } else {
        echo "<p>Không có thông tin đăng ký để lưu.</p>";
    }
}
?>
