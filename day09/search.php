<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tìm kiếm danh sách sinh viên</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./styles/styles_search.css">
</head>

<body>
    <div class="container">
        <div class="search-box">
            <form method="post" id="search-form">
                <div class="department">
                    <label for="department">Khoa</label>
                    <select name="depts" id="getDept">
                        <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                        <option value="MAT">Khoa học máy tính</option>
                        <option value="KDL">Khoa học vật liệu</option>
                    </select>
                </div>
                <div class="search">
                    <label for="search">Từ khoá</label>
                    <input type="text" id="getName">
                </div>
                <div class="btn">
                    <button id="btn" type="submit" value="submit" name="search">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <p id="resultCount"></p>
        <p id="no"></p>
        <table id="student-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);

                include 'database.php';

                $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $sql = "SELECT full_name, department, id FROM STUDENTS";
                $result = $conn->query($sql);

                $i = 1;
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    echo "<tr>";
                    echo "<td>" . $i . "</td>";
                    echo "<td>" . $row["full_name"] . "</td>";
                    echo "<td>" . $row["department"] . "</td>";
                    echo "<td>";
                    echo '<button class="button-container delete-button" data-student-id="' . $row["id"] . '">Xóa</button>';
                    echo '<button class="button-container edit-button" data-student-id="' . $row["id"] . '">Sửa</button>';
                    echo "</td>";
                    echo "</tr>";
                    $i++;
                }

                $conn = null; // Close the PDO connection here
                ?>
            </tbody>
        </table>
    </div>
    <script src="./scripts/scripts_search.js"></script>
</body>

</html>
