<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link rel="stylesheet" type="text/css" href="./styles/styles_register.css">
</head>
<body>
    <div class="form-container">
        <div id="error-message" class="error-message"></div>
        <form id="registration-form" method="POST" action="confirm.php">
            <div id="error-container"></div>
            <div class="register-form">
                <label for="full-name" class="form-label">Họ và tên <span class="required"> *</span></label>
                <input type="text" id="full-name-input" name="full-name" placeholder="">
                <input type="hidden" name="full-name" id="hidden-full-name">
            </div>
            <div class="register-form">
                <label class="form-label">Giới tính <span class="required"> *</span></label>
                <input type="hidden" name="gender" id="hidden-gender">
                <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');
                    for ($i = 0; $i < count($genders); $i++) {
                        $gender = $genders[$i];
                        echo '<input type="radio" id="gender-' . $i . '" name="gender" value="' . $gender . '">';
                        echo '<label for="gender-' . $i . '">' . $gender . '</label>';
                    }
                ?>
            </div>
            <div class="register-form" >
                <label for="department" class="form-label">Phân khoa <span class="required"> *</span></label>
                <input type="hidden" name="department" id="hidden-department">
                <select id="department" name="department">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                        $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                        foreach ($departments as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                    ?>
                </select>
            </div>
            <div class="register-form">
                <label for="date-of-birth" class="form-label"> Ngày sinh <span class="required">*</span></label>
                <input type="text" id="date-of-birth-input" name="date-of-birth" placeholder="dd/mm/yyyy" onblur="validateDate()"> 
                <input type="hidden" name="date-of-birth" id="hidden-date-of-birth">
            </div>
            <div class="register-form" style="align-items: flex-start;">
                <label for="address" class="form-label"> Địa chỉ </label>
                <input type="text" id="address" name="address" placeholder="">
                <input type="hidden" name="address" id="hidden-address">
            </div>
            <div class="register-form">
                <label for="image" class="form-label">Hình ảnh</label>
                <input type="file" id="image" name="image" style="display: none;">
                <input type="hidden" name="image" id="hidden-image">
                <label for="image" class="custom-file-upload">Choose File</label><span id="file-name">No file chosen</span>
                <img id="selected-image" src="" alt="Selected Image" style="max-width: 25%; display: none;">
            </div>
            <button type="submit" id="submit-button">Đăng ký</button>
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
            <script type="text/javascript" src="./scripts/scripts_register.js"></script>
        </form>
    </div>
</body>
</html>
